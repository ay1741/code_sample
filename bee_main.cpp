#include <iostream>
#include <time.h>
#include "world/beehive.h"
#include <stdlib.h>     //for using the function sleep
#include <zconf.h>

using namespace std;

int main(int argc, char* argv[])
{
    if(argc != 6)
    {
        cout << "Usage: bee_main seed seconds drones nectar_workers pollen_workers" << endl;
    }
    else
    {
        //seed, seconds, drones, nectar workers, pollen workers
        world::beehive *b1 = new world::beehive(stoull(argv[1]), stoi(argv[2]), stoi(argv[3]), stoi(argv[4]), stoi(argv[5]));
      //  world::beehive *b1 = new world::beehive(0, 15, 4, 4, 4);

        b1->start_sim();

        sleep(stoi(argv[2]));
        //sleep(15);

        b1->end_sim();

    }

    return 0;
}
