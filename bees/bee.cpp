#include "bee.h"
using std::exception;
using bees::bee;

bees::bee::bee(role role, int id): role1_(role), id_(id), thread_{}
{}

bees::bee::~bee() {
    if (thread_.joinable()) {
        thread_.join();
    }
}

void bees::bee::start() {
    thread_ = thread{&bee::run, this};
}

void bees::bee::join() {
    thread_.join();
}

int bees::bee::get_id() {
    return id_;
}

string bees::bee::getRole()
{
    if(role1_ == drone)
    {
        return "DRONE";
    }
    else if(role1_ == worker)
    {
        return "WORKER";
    }
    else
    {
        return "QUEEN";
    }
};