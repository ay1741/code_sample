#include <string>
#include <iostream>
#include <thread>

#include "../util/random.h"
#include "../util/my_thread.h"

using std::string;
using std::cerr;
using std::cout;
using std::endl;
using std::exception;
using std::chrono::milliseconds;
using std::this_thread::sleep_for;
using std::thread;

#ifndef PROJECT2_BEE_H
#define PROJECT2_BEE_H

namespace bees {

    /**
     * A class to represent a bee
     *
     * @author Alexander Young
     */

    class bee {
    public:

        //Simple enum for the different types of bees
        enum role { worker, drone, queen};

        /**
         * Destruct the bee.  The behavior is if the thread has not been
         * joined on yet it will when this is run.
         */
        virtual ~bee();

        /**
         * Calls the run method for the bee
         */
        void start();

        /**
         * Calls the join method for the bee
         */
        void join();

        /**
        bee constructor: Creates a new bee object thread

        @param role - role of the bee
        @param id - id of the bee
         */
        bee(role role, int id);

        /**
        * Local run method for bee thread, to be overridden by each bee type
        */
        virtual void run() = 0;

        /**
        Provides the id of the bee

        @return id of the bee
        */
        int get_id();

        /**
        Provides the role of bee

        @return String value of role of bee
        */
        string getRole();

    protected:
        /** id of the bee */
        int id_;

        /** role of the bee */
        const role role1_;

        /** thread of the bee */
        thread thread_;

    }; //bee
} // bee
#endif //BEE_H
