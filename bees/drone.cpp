#include "drone.h"
#include "../world/beehive.h"
#include "../world/queens_chamber.h"

using namespace std;

bees::drone::drone(bees::bee::role role, world::queens_chamber *qc1, world::beehive *bh1, int id)
        : bee(role, id), bh1_(bh1), qc1_(qc1), mate(false)
{
    cout << "*B* Drone #" << id << " is born" << endl;
}

void bees::drone::run() {
    if (bh1_->getStatus() == true)
    {
        qc1_->enter(this);
    }
}

void bees::drone::set_mate(bool in)
{
    mate = in;
}