#include "bee.h"
#include "../util/random.h"
#include "../world/flower_field.h"

#ifndef PROJECT2_DRONE_H
#define PROJECT2_DRONE_H

namespace world{
    class queens_chamber;
    class beehive;
} // world

namespace bees {

    /**
     * A class to represent a drone bee, inherits from the bee class
     *
     * @author Alexander Young
     */

    class drone : public bees::bee {
    public:

        /**
        drone constructor: Creates a new drone bee

        @param role - role of the bee
        @param qc1 - pointer to the queens chamber
        @param bh1 - pointer to the beehive
        @param id - id of the bee
         */
        drone(bees::bee::role role, world::queens_chamber *qc1, world::beehive *bh1, int id);

        /**
         * Custom run method for the drone bee
         */
        void run();

        /**
        * A method to be used to set the mate flag for the drone
        *
        * @param in - set the flag high or low
        */
        void set_mate(bool in);

    protected:

        //Flag to mark if the bee has mated yet
        bool mate;

        //Pointer to the queens chambers where the drone enters to mate
        world::queens_chamber *qc1_;

        //Pointer to the beehive
        world::beehive *bh1_;

    }; //drone
} //bees

#endif