#include "queen.h"
#include "../world/beehive.h"
#include "../world/queens_chamber.h"

using namespace std;

bees::queen::queen(bees::bee::role role, world::queens_chamber *qc1, world::beehive *bh1, int id)
        : bee(role, id), qc1_(qc1),bh1_(bh1), lg2_(std::cout)
{
    cout << "*B* QUEEN #1 is born" << endl;
}

void bees::queen::run()
{
    while(bh1_->getStatus() == true) {

        if(bh1_->get_current_nectar() > 0 && bh1_->get_current_pollen() > 0 && qc1_->is_empty() == false)
        {
            int mate = 0;

            if(bh1_->get_current_nectar() >= 5 && bh1_->get_current_pollen() >= 5)
            {
                bh1_->subtract_nectar(5);
                bh1_->subtract_pollen(5);

                mate = 5;

            } else
            {
                mate = std::min(bh1_->get_current_nectar(), bh1_->get_current_pollen());
                bh1_->subtract_nectar(mate);
                bh1_->subtract_pollen(mate);
            }

            qc1_->mate(this);

            sleep_for(milliseconds(2000));

            for(int i = 0; i < mate; i++)
            {
                int rand = bh1_->roll_dice(1,10);

                if(rand <= 6)
                {
                    bh1_->make_drone();
                }
                else if(rand > 6 and rand < 9)
                {
                    bh1_->make_nectar_worker();
                }
                else
                {
                    bh1_->make_pollen_worker();
                }
            }

            string hold = "*Q* Queen birthed " + std::to_string(mate) + " children";
            lg2_.log(hold);

            sleep_for(milliseconds(1000));
        }
    }
}