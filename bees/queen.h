#include "bee.h"
#include "../util/random.h"
#include "../world/flower_field.h"
#include "../util/logger.h"

#ifndef PROJECT2_QUEEN_H
#define PROJECT2_QUEEN_H

namespace world{
    class queens_chamber;
    class beehive;
} //world

namespace bees {

    /**
     * A class to represent a queen bee, inherits from the bee class
     *
     * @author Alexander Young
     */

    class queen : public bees::bee {
    public:

        /**
         queen constructor: Creates a new queen bee

         @param role - role of the bee
         @param qc1 - pointer to the queens chamber
         @param bh1 - pointer to the beehive
         @param id - id of the bee
          */
        queen(bees::bee::role role, world::queens_chamber *qc1, world::beehive *bh1, int id);

        /**
         * Custom run method for the queen bee
         */
        void run();

    protected:

        //Pointer to the queens chambers where the queen lives
        world::queens_chamber *qc1_;

        //Pointer to the beehive
        world::beehive *bh1_;

        //Pointer to thread safe logger
        util::logger lg2_;
    }; //queen
} //bees

#endif