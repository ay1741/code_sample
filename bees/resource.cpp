#include "resource.h"

using bees::resource;

resource::resource(Type type)
{
    _type = type;
}

resource::Type resource::get_type() const
{
    return _type;
}

resource::operator string() const
{
    switch(get_type()){
        case NECTAR:
            return "NECTAR";
        case POLLEN:
            return "POLLEN";
    }

    return "";
}