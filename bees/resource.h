#include <string>

using std::string;

#ifndef PROJECT2_RESOURCE_H
#define PROJECT2_RESOURCE_H

namespace bees{

    /**
     * A class to represent the different resources the worker bee can collect
     *
     * @author Alexander Young
     */
    class resource {
    public:

        /**
         *Custom type to store the different resources
         */
        enum Type {NECTAR, POLLEN} enumField;

        /*
       * Constructor for resource type
       *
       * @param Type
       *
       */
        resource(Type type);

        /*
       * Get the type of the resource
       *
       * @return Type
       *
       */
        Type get_type() const;

        /*
        *String operator of resource class, to return the type in string format
        *
       */
        operator string() const;

    private:
        Type _type;
    }; //resource
} //bees

#endif
