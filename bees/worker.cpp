#include "worker.h"
#include "../world/beehive.h"
#include "resource.h"

using namespace std;

bees::worker::worker(bees::bee::role role, bees::resource rs1, unsigned int amount_collect, world::flower_field *fd1, world::beehive *bh1, int id)
        : bee(role, id), rs1_(rs1), amount_to_collect_(amount_collect), fd1_(fd1),bh1_(bh1), lg2_(std::cout), dead(false)
{
    cout << "*B* " << string(rs1_) << "(" << amount_to_collect_ << ")" << " WORKER #" << id_ << " is born" << endl;
}

bees::resource bees::worker::getResource()
{
    return rs1_;
}

int bees::worker::getAmountToCollect()
{
    return amount_to_collect_;
}

void bees::worker::set_death(bool dead1){
    dead = dead1;
}

void bees::worker::run()
{
    while(bh1_->getStatus() == true && dead == false) {
        fd1_->enter(this);
        sleep_for(milliseconds(1000) * getAmountToCollect());
        fd1_->exit(this);

        if (string(rs1_) == "NECTAR") {
            bh1_->add_nectar(getAmountToCollect());
            string hold = "*BH* NECTAR(" + std::to_string(amount_to_collect_) + ") WORKER #" + std::to_string(id_) + " deposits resources";
            lg2_.log(hold);

            bh1_->life_or_death(this);
        }

        if (string(rs1_) == "POLLEN") {
            bh1_->add_pollen(getAmountToCollect());
            string hold = "*BH* POLLEN(" + std::to_string(amount_to_collect_) + ") WORKER #" + std::to_string(id_) + " deposits resources";
            lg2_.log(hold);

            bh1_->life_or_death(this);
        }
    }
}