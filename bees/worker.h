#include "bee.h"
#include "../util/random.h"
#include "resource.h"
#include "../util/logger.h"

#ifndef PROJECT2_WORKER_H
#define PROJECT2_WORKER_H

namespace world{
    class flower_field;
    class beehive;
} //world

namespace util
{
    class logger;
} //util

namespace bees {

    /**
     * A class to represent a worker bee, inherits from the bee class
     *
     * @author Alexander Young
     */
    class worker : public bees::bee {
    public:

        /**
         worker constructor: Creates a new worker bee

         @param role - role of the bee
         @param rs1 - the resource the bee instance collects
         @param amount_collect - how much of that resource the bee collects each time
         @param fd1 - pointer to the flower field
         @param bh1 - pointer to the beehive
         @param id - id of the bee
          */
        worker(role role, resource rs1, unsigned int amount_collect, world::flower_field *fd1, world::beehive *bh1, int id);

        /**
         * Get the resource the worker bee collects
         *
         * @return resource type
         */
        resource getResource();

        /**
        * Get the amount of resources the worker farms
        *
        * @return resource count
        */
        int getAmountToCollect();

        /**
         * Custom run method for the worker bee
         */
        void run();

        /**
         * A method where one can set the dead flag if the queen has died
         */
        void set_death(bool dead1);

        protected:

        //Store the random number of resources to collect
        unsigned int amount_to_collect_;

        //Pointer to the flower field where worker bees can get resources
        world::flower_field *fd1_;

        //Pointer to the beehive
        world::beehive *bh1_;

        //The resource this instance of the worker bee collects
        resource rs1_;

        //A flag to mark if the queen has died
        bool dead;

        //Pointer to thread safe logger
        util::logger lg2_;


    }; //worker
} //bees

#endif