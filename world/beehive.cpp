#include <algorithm>
#include "beehive.h"

using namespace std;

using world::beehive;

beehive::beehive (unsigned long long seed, int seconds, int drones, int nectar_workers, int pollen_workers)
        : rand1_(seed), run_time(seconds), drones_(drones), nectar_workers_(nectar_workers), pollen_workers_(pollen_workers), f1(), active(true), total_pollen(0), total_nectar(0), counter(1), current_pollen(0), current_nectar(0),lg1_(std::cout), bee_vec(), dead_bees(), lk(), mtx(), qc()
{
    cout << "Seed: " << seed << endl;
    cout << "Simulation time: " << seconds << " seconds" << endl;
    cout << "Starting drones: " << drones << endl;
    cout << "Starting nectar workers: " << nectar_workers << endl;
    cout << "Starting pollen workers: " << pollen_workers << endl;

    qc = new queens_chamber(this);
}

beehive::~beehive() {}

void beehive::start_sim()
{

    bee_vec.emplace_back(new bees::queen(bees::bee::role::queen, qc, this, counter));
    counter++;

    for(int i = 0; i < drones_; i++) {

        bee_vec.emplace_back(new bees::drone(bees::bee::role::drone, qc, this, counter));
        counter++;
    }

    for(int i = 0; i < nectar_workers_; i++) {

        unsigned int hold = roll_dice(1,6);
        bee_vec.emplace_back(new bees::worker(bees::bee::role::worker, bees::resource::NECTAR, hold, &f1, this, counter));
        counter++;
    }

    for(int i = 0; i < pollen_workers_; i++) {

        unsigned int hold = roll_dice(1, 6);
        bee_vec.emplace_back(new bees::worker(bees::bee::role::worker, bees::resource::POLLEN, hold, &f1, this, counter));
        counter++;
    }

    cout << "*BH* Beehive begins buzzing!" << endl;

    for(int i = 0; i < bee_vec.size(); i++)
    {
        bee_vec.at(i)->start();
    }

}

void beehive::end_sim()
{
    active = false;

    cout << "=============================" << endl;
    cout << "END SIM" << endl;
    cout << "=============================" << endl;

    //int x = 0;
    while(!qc->is_empty())
    {
        qc->dismiss_drone();
    }

       while(!bee_vec.empty()) {

           mtx.lock();

           if(bee_vec.back()->getRole() == "DRONE")
           {
               bee_vec.pop_back();
           }
           else
           {
               //cout << "Size: " << bee_vec.size() << endl;
               //cout << "Bee: " << bee_vec.back()->get_id() << endl;
               bee_vec.back()->join();
               bee_vec.pop_back();
           }
           mtx.unlock();


        //cout << "X: " << x << endl;
        //x++;
    }

    cout << "*BH* Beehive stops buzzing!" << endl << endl;
    cout << "STATISTICS" << endl;
    cout << "==========" << endl;
    cout << "Bees born: " << counter - 1 << endl;
    cout << "Bees perished: " << dead_bees.size() << endl;

    for(int i = 0; i < dead_bees.size(); i++)
    {
        cout << dead_bees.at(i) << endl;
    }

    cout << "Nectar remaining " << current_nectar << endl;
    cout << "Pollen remaining " << current_pollen << endl;
    cout << "Nectar gathered " << total_nectar << endl;
    cout << "Pollen gathered " << total_pollen << endl;
}

unsigned int beehive::roll_dice(unsigned int min, unsigned int max){
    return rand1_.roll_dice(min, max);
}

bool beehive::getStatus()
{
    return active;
}

void beehive::add_nectar(int total) {
    std::atomic_fetch_add(&total_nectar, total);
    std::atomic_fetch_add(&current_nectar, total);
}

void beehive::add_pollen(int total) {
    std::atomic_fetch_add(&total_pollen, total);
    std::atomic_fetch_add(&current_pollen, total);

}

void beehive::life_or_death(bees::worker *worker)
{
    if(current_nectar.load() > 0 && current_pollen.load() > 0)
    {
        subtract_nectar(1);
        subtract_pollen(1);

        string hold = "*W* " + string(worker->getResource()) + "(" + std::to_string(worker->getAmountToCollect()) + ") WORKER #" + std::to_string(worker->get_id()) + " is refueling";

        lg1_.log(hold);
    }
    else
    {
        worker->set_death(true);

        string hold = "*BH* " + string(worker->getResource()) + "(" + std::to_string(worker->getAmountToCollect()) + ") WORKER #" + std::to_string(worker->get_id()) + " perished!";

        lg1_.log(hold);

        dead_bees.emplace_back("\t" + string(worker->getResource()) + "(" + std::to_string(worker->getAmountToCollect()) + ") WORKER #" + std::to_string(worker->get_id()));
    }
}

void beehive::subtract_nectar(int total)
{
    std::atomic_fetch_sub(&current_nectar, total);
}

void beehive::subtract_pollen(int total)
{
    std::atomic_fetch_add(&current_pollen, total);
}

int beehive::get_current_pollen()
{
    return current_pollen.load();
}

int beehive::get_current_nectar()
{
    return current_nectar.load();
}

void beehive::make_pollen_worker()
{
    if(active == true) {
        unsigned int hold = roll_dice(1, 6);
        bee_vec.emplace_back(
                new bees::worker(bees::bee::role::worker, bees::resource::POLLEN, hold, &f1, this, counter));

        mtx.lock();
        bee_vec.back()->start();
        mtx.unlock();

        counter++;
    }
}

void beehive::make_nectar_worker()
{
    if(active == true) {
        unsigned int hold = roll_dice(1, 6);
        mtx.lock();
        bee_vec.emplace_back(
                new bees::worker(bees::bee::role::worker, bees::resource::NECTAR, hold, &f1, this, counter));
        bee_vec.back()->start();
        mtx.unlock();
        counter++;
    }
}

void beehive::make_drone()
{
    if(active == true) {
        bee_vec.emplace_back(new bees::drone(bees::bee::role::drone, qc, this, counter));
        bee_vec.back()->start();
        counter++;
    }
}

void beehive::kill_drone(bees::drone *drone)
{
    string hold = "*BH* DRONE #" + std::to_string(drone->get_id()) + " perished!";
    lg1_.log(hold);

    dead_bees.emplace_back("\tDRONE #" + std::to_string(drone->get_id()));
}