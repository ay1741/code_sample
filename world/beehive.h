#ifndef PROJECT2_BEEHIVE_H
#define PROJECT2_BEEHIVE_H

#include "../bees/worker.h"
#include "../bees/queen.h"
#include "../bees/drone.h"
#include "../util/logger.h"

#include "queens_chamber.h"
#include "flower_field.h"

#include <iostream>
#include <list>
#include <iterator>

namespace world {

    /**
     * A class to represent a beehive
     *
     * @author Alexander Young
     */
    class beehive {
    public:

        /**
         Beehive Constructor: Creates a new behive instance

         @param seed - seed for random number generator
         @param seconds - seconds to run the simulation for
         @param drones - number of drone bees to start with
         @param nectar_workers - number of nectar worker bees to start with
         @param pollen_workers - number of pollen worker bees to start with
         */
        beehive(unsigned long long seed, int seconds, int drones, int nectar_workers, int pollen_workers);

        /**
         * Beehive destructor
         */
        ~beehive();

        /**
         *  Public flag to check if the beehive is active
         */
        bool active;

        /**
         Provides a function to roll a dice (random number generator)
         @param min - smallest number one can roll
         @param max - largest number one can roll

         @return random integer number
         */
        unsigned int roll_dice(unsigned int min, unsigned int max);

        /*
         * Function to start the beehive
         */
        void start_sim();

        /*
         * Fucntion to end the simulation
         */
        void end_sim();

        /*
         * Function to check if the current status of the simulation
         *
         * @return If the sim is currently running
         */
        bool getStatus();

        /*
         * Add a certain amount of nectar to the beehive
         *
         * @param total - The amount of nectar to add
         */
        void add_nectar(int total);

        /*
         * Subtract a certain amount of nectar from the beehive
         *
         * @param total - The amount of nectar to subtract
         */
        void subtract_nectar(int total);

        /*
         * Get the total amount of nectar in the beehive
         *
         * @return Total amount of nectar
         */
        int get_current_nectar();

        /*
         * Add a certain amount of pollen to the beehive
         *
         * @param total - The amount of pollen to add
         */
        void add_pollen(int total);

        /*
        * Subtract a certain amount of pollen from the beehive
        *
        * @param total - The amount of pollen to subtract
        */
        void subtract_pollen(int total);

        /*
         * Get the total amount of pollen in the beehive
         *
         * @return Total amount of pollen
         */
        int get_current_pollen();

        /*
         * Function to determine if the worker bee should live
         *
         * @param worker - worker bee pointer
         */
        void life_or_death(bees::worker *worker);

        /*
         * Create a pollen worker
         */
        void make_pollen_worker();

        /*
         * Create a nectar worker
         */
        void make_nectar_worker();

        /*
         * Create a drone bee
         */
        void make_drone();

        /*
         * Kill a drone bee
         */
        void kill_drone(bees::drone *drone);

    protected:

        //Random number generator object
        util::random rand1_;

        //Variable to store the total run time
        int run_time;

        //Number of drones currently alive
        int drones_;

        //Number of worker bees collecting nectar
        int nectar_workers_;

        //Numnber of worker bees collecting pollen
        int pollen_workers_;

        //Flower field instance for worker bees to enter
        flower_field f1;

        //Queens chamber instance where the queen mates with drones
        queens_chamber *qc;

        //Vector holding bee pointers that are alive
        std::vector<bees::bee*> bee_vec;

        //Vector holding a string of dead bees to print out at the end
        std::vector<string> dead_bees;

        //List - update comment
        std::list <int> lk;

        //Number of pollen resources currently available
        std::atomic<int> current_pollen;

        //Number of nectar resources currently available
        std::atomic<int> current_nectar;

        //Stores the total number of pollen collected during the simulation
        std::atomic<int> total_pollen;

        //Stores the total number of pollen collected during the simulation
        std::atomic<int> total_nectar;

        //Thread mutex
        std::mutex mtx;

        //General counter
        int counter;

        //Thread safe logger to print to console
        util::logger lg1_;

    }; //beehive
} //world

#endif