#include "flower_field.h"
#include "beehive.h"

using namespace world;

flower_field::flower_field(): lg1_(std::cout), worker_mutex(), worker_cond(), current_workers(0) {}

void flower_field::enter(bees::worker *worker)
{
    string hold = "*FF* " + string(worker->getResource()) + "(" + std::to_string(worker->getAmountToCollect()) + ") WORKER #" + std::to_string(worker->get_id()) + " enters field";

    lg1_.log(hold);

    unique_lock<mutex> ul{worker_mutex};

    //worker_cond.wait(ul);
    worker_cond.wait(ul, [this, worker]{ return current_workers < MAX_WORKERS;});

    current_workers = current_workers + 1;

    //cout << "Workers: " << current_workers << endl;

}

void flower_field::exit(bees::worker *worker)
{

    string hold = "*FF* " + string(worker->getResource()) + "(" + std::to_string(worker->getAmountToCollect()) + ") WORKER #" + std::to_string(worker->get_id()) + " leaves field";

    lg1_.log(hold);

    lock_guard<mutex> lg{worker_mutex};

    current_workers = current_workers - 1;

    worker_cond.notify_all();

}