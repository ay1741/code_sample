#ifndef PROJECT2_FLOWER_FIELD_H
#define PROJECT2_FLOWER_FIELD_H

#include <condition_variable>
#include <mutex>
#include <future>
#include <thread>
#include <iostream>
#include <queue>
#include "../bees/worker.h"
#include "../util/logger.h"

using std::chrono::milliseconds;
using std::condition_variable;
using std::cout;
using std::endl;
using std::lock_guard;
using std::mutex;
using std::queue;
using std::this_thread::sleep_for;
using std::thread;
using std::unique_lock;

namespace world
{

    /**
     * A class to represent the flower field
     *
     * @author Alexander Young
     */
    class flower_field
    {
    public:

        /*
         * Flower Field Constructor
         */
        flower_field();

        /*
         * Allow the worker bees to enter the flower field
         *
         * @param worker - pointer to worker bee
         */
        void enter(bees::worker *worker);

        /*
         * Allow the worker bee to exit the flower field
         *
         * @param worker - pointer to worker bee
         */
        void exit(bees::worker *worker);

    protected:

        //Constant to store the max bees allowed in the flower field
        static const int MAX_WORKERS = 10;

        //Number of workers currently in the the flower field
        unsigned int current_workers;

        //Worker mutex
        mutex worker_mutex;

        //Condition variable for the worker bee
        condition_variable worker_cond;

        //Instance of the thread safe logger (console output)
        util::logger lg1_;
    };//flower_field
}//world

#endif