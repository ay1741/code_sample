#include <functional>
#include "queens_chamber.h"
#include "beehive.h"

using namespace world;

queens_chamber::queens_chamber(beehive *bh1) : lg1_(std::cout), bh1(bh1), queen_here(false)
{}

void queens_chamber::enter(bees::drone *drone)
{

    string hold = "*QC* Drone #" + std::to_string(drone->get_id()) + " enters chamber";

    lg1_.log(hold);

    unique_lock<mutex> ul{queue_mutex};
    q.emplace_back(drone);
    queue_cond.wait(ul, [&]{ return (queen_here && q.front() == drone) || (bh1->getStatus() == false);});

    q.pop_front();

    queen_here = false;

    hold = "*QC* Drone #" + std::to_string(drone->get_id()) + " exits chamber";

    lg1_.log(hold);
}

void queens_chamber::mate(bees::queen *queen)
{
  //  if(!q.empty()) {
        lock_guard<mutex> lg{queue_mutex};

        queen_here = true;

        queue_cond.notify_all();

        q.front()->set_mate(true);

        //*QC* Queen mates with DRONE #2

        string hold = "*QC* Queen mates with DRONE # " + std::to_string(q.front()->get_id());

        lg1_.log(hold);

        bh1->kill_drone(q.front());

        //string hold = "*QC* Drone #" + std::to_string(q.front()->get_id()) + " exits chamber";

        q.pop_front();

        queen_here = false;
   // }
}

bool queens_chamber::is_empty()
{
    return q.empty();
}

int queens_chamber::queue_size()
{
    return q.size();
}

void queens_chamber::dismiss_drone()
{
    //lock_guard<mutex> lg(queue_mutex);

    queen_here = true;

    queue_cond.notify_one();

    //string hold = "*QC* Drone #" + std::to_string(q.front()->get_id()) + " exits chamber";

    //lg1_.log(hold);

    //q.pop_front();

    queen_here = false;

    //string hold = "Queue size: " + std::to_string(q.size());

    //lg1_.log(hold);
}