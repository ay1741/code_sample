#ifndef PROJECT2_QUEENS_CHAMBER_H
#define PROJECT2_QUEENS_CHAMBER_H

#include <condition_variable>
#include <mutex>
#include <future>
#include <thread>
#include <iostream>
#include <queue>
#include "../bees/drone.h"
#include "../bees/queen.h"
#include "../util/logger.h"
#include "beehive.h"

using std::chrono::milliseconds;
using std::condition_variable;
using std::cout;
using std::endl;
using std::lock_guard;
using std::mutex;
using std::queue;
using std::this_thread::sleep_for;
using std::thread;
using std::unique_lock;

namespace world
{

    /**
     * A class to represent the queens chamber
     *
     * @author Alexander Young
     */
    class queens_chamber
    {
    public:

        /*
         * Constructor
         *
         * @param bh1 - pointer to the beehive
         */
        queens_chamber(beehive *bh1);

        /*
         * Allow the drones to enter the beehive
         *
         * @param drone - pointer to a drone bee
         */
        void enter(bees::drone *drone);

        /*
         * Allow the queen to mate with a drone bee
         *
         * @param queen - pointer to the queen bee
         */
        void mate(bees::queen *queen);

        /*
         * Check if there are any drones left in the chamber
         *
         * @return flag if the chamber is empty or not
         */
        bool is_empty();

        /*
         * Dismiss a drone from the queens chamber
         */
        void dismiss_drone();

        /*
         * Get the size of the queue
         *
         * @return queue size
         */
        int queue_size();

    protected:

        //Mutex to lock the queue
        mutex queue_mutex;

        //Flag to set if the queen is in the chamber
        bool queen_here;

        //Condition variable for the queue
        condition_variable queue_cond;

        //Deque of drone bee pointers
        std::deque<bees::drone*> q;

        //Pointer to the beehive
        beehive *bh1;

        //Thread safe logger
        util::logger lg1_;
    }; //queens chamber
} //world

#endif